# Vim Spec Layout

** Supports rspec and qunit only **

When your spec files get too large, it becomes hard to find where to put new
ones. This plugin grabs all the spec description lines with indentation kept, so
that that task becomes a little bit easier.

## Installation
I use vim-plug, but any of the popular installers should work. There's always
manually dropping it into your vim directory as well.

With vim-plug, you'll have to type in the full url of this plugin's Gitlab
repository, since the default is for Github.

The plugin makes use of grep, so you'll also have to have that installed.

## Usage
Just call `SpecLayout`. A new buffer opens up with the results. There's nothing
fancy about it.

Alternatively, `SpecLayout name_of_test_framework` in case the auto-detection
doesn't work, but if you've read the bolded statement above on which frameworks
are detected and you have sane file extensions, you should be fine with just
`SpecLayout`.

## Future Test Framework Support
Unlikely to happen unless I personally end up needing it.

## Find a bug?
Open a pull request! Or start an issue. Or email me (gary@garymei.com).

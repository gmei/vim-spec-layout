command! -nargs=* SpecLayout call <SID>SpecLayout(<f-args>)

let s:supportedTypes = ["rspec", "qunit"]

function! s:SpecLayout(...)
    let type = a:0 ? a:1 : s:InferType(expand('%:e'))

    if !s:SupportedType(type)
        echo "Unsupported type: " . type
        return
    endif

    let filePath = @%
    " Create a scratch buffer so that closing doesn't prompt to save
    call s:NewScratch()
    if type ==? "rspec"
        call s:MatchRSpec(filePath)
    elseif type ==? "qunit"
        call s:MatchQUnit(filePath)
    end
endfunction

function! s:MatchRSpec(filePath)
    " Normal describe/context/it blocks are in the format
    " `describe 'something' do`
    " with variations such as
    " `it { is_expected... }`

    let firstCondition = '\(describe\\|context\\|it\)\s\+\(\"\\|''\\|{\).*\(\"\\|''\\|}\)'
    " This is for the start of an RSpec block, where
    " `describe ClassUnderTest do`
    " is the expected format
    let secondCondition = '\(describe\s\+[^ ]\+\)'
    let conditions = '\(' . firstCondition . '\\|' . secondCondition . '\)'
    silent execute '%!grep -o "\b\s*' . conditions . '"' . ' ' . a:filePath
endfunction

function! s:MatchQUnit(filePath)
    " Normal QUnit.module/test blocks are in the format
    " QUnit.module('something', function () {...

    let condition = '\(QUnit\.module\\|test\)(\(\"\\|''\).*\(\"\\|''\)'
    silent execute '%!grep -o "\b\s*' . condition . '"' . ' ' . a:filePath
endfunction

function! s:InferType(fileExtension)
    " There's a lot of testing frameworks out there, but I was having
    " trouble storing the results of `%s/pattern//ng` into a variable
    " to compare with, so this was the fastest way to solve my
    " problem for now.
    if a:fileExtension ==? "rb"
        return "rspec"
    elseif a:fileExtension == "js"
        return "qunit"
    endif

    return "Could not infer spec type"
endfunction

function! s:SupportedType(type)
    for supportedType in s:supportedTypes
        if a:type ==? supportedType
            return 1
        endif
    endfor

    return 0
endfunction

function! s:NewScratch()
    enew
    setlocal buftype=nofile
    setlocal bufhidden=hide
    setlocal noswapfile
    set filetype=ruby
endfunction
